import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, GuildMember, Role, User} from "discord.js";
import {Task} from "../entity/task";
import {Board} from "../entity/board";

export class Progress {

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('progress')
            .setDescription('Move a task along')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose the board the task is in')
                .setRequired(true))
            .addStringOption(builder => builder.setName('task')
                .setDescription('Choose which task to progress')
                .setRequired(true))
    }

    public static handleInteraction(interaction: CommandInteraction) {
        const mentionable = interaction.options.getMentionable('board');

        let snowflake;
        if (mentionable instanceof GuildMember || mentionable instanceof User || mentionable instanceof Role ) {
            snowflake = mentionable.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        const task = interaction.options.getString('task');

        Board.findOne({where: {mentionable: snowflake}}).then(board => {
            Task.findOne({where: {id: task, board: board }}).then(task => {
                if(task.status === "NEW") {
                    task.status = "IN_PROGRESS";
                    task.save().then(task => interaction.reply("Task is now 'In Progress'"));
                }
                else if(task.status === "IN_PROGRESS"){
                    task.status = "DONE";
                    task.save().then(task => interaction.reply("Task is now 'Done'"));
                }
                else{
                    interaction.reply("Can't progress task any further");
                }
            }).catch(() => {
                interaction.reply(`Could not find task ${task}`);
            });
        }).catch(() => {
            interaction.reply(`Could not find the board`);
        })

    }

}