import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, GuildMember, Role, User} from "discord.js";
import {Task} from "../entity/task";
import {Log} from "../service/log";

export class New {

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('new')
            .setDescription('Create a new task')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose to which persons or roles board this task get added')
                .setRequired(true))
            .addStringOption(builder => builder.setName('task_name')
                .setDescription('Outline what the task is about')
                .setRequired(true))
            .addStringOption(builder => builder.setName('priority')
                .setDescription('Manually choose the priority of the task')
                .addChoice('High','HIGH')
                .addChoice('Medium','MEDIUM')
                .addChoice('Low','LOW'))
            .addStringOption(builder => builder.setName('due_date')
                .setDescription('Give an ISO 8601 formatted date that the task should be done by'));
    }

    public static handleInteraction(interaction: CommandInteraction){
        const mentionable = interaction.options.getMentionable('board');

        let snowflake;
        if (mentionable instanceof GuildMember || mentionable instanceof User || mentionable instanceof Role ) {
            snowflake = mentionable.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        const description = interaction.options.getString('task_name');
        let priority = interaction.options.getString('priority');
        if(!priority)
            priority = 'LOW';
        let dueDate;
        try {
            dueDate = Date.parse(interaction.options.getString('due_date'));
        }  catch (e){
            interaction.reply(`${interaction.options.getString('due_date')} is not a valid date. Please format it like this YYYY-MM-DDTHH:mm:ss`)
            return;
        }
        const task = new Task(snowflake,description,priority,dueDate);
        task.ready.then(() => {
            //console.log(task.board);
            task.save().then(task => {
                interaction.reply(`Task "${task.description}" was created`);
            });
        });
    }
}