import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, GuildMember, Role, User} from "discord.js";
import {Board} from "../entity/board";
import {Task} from "../entity/task";
import {Database} from "../service/database";

export class Assign{

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('assign')
            .setDescription('Move a task to a different board')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose from which board the task should get moved')
                .setRequired(true))
            .addStringOption(builder => builder.setName('task')
                .setDescription('Choose which task to progress')
                .setRequired(true))
            .addMentionableOption(builder => builder.setName('new_board')
                .setDescription('Choose to which persons or roles board this task get moved'));
    }

    public static handleInteraction(interaction: CommandInteraction) {

        const mentionable = interaction.options.getMentionable('board');

        let snowflake;
        if (mentionable instanceof GuildMember || mentionable instanceof User || mentionable instanceof Role ) {
            snowflake = mentionable.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        const task = interaction.options.getString('task');

        const mentionableNew = interaction.options.getMentionable('new_board');

        let snowflakeNew;
        if (mentionableNew instanceof GuildMember || mentionableNew instanceof User || mentionableNew instanceof Role ) {
            snowflakeNew = mentionableNew.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        Board.findOne({where: {mentionable: snowflake}}).then(board => {
            Task.findOne({where: {id: task, board: board }}).then(task => {
                task.remove().then(() => {
                    Board.findOne({where: {mentionable: snowflakeNew}}).then(newBoard => {
                        task.board = newBoard;
                        task.save().then(task => {
                            interaction.reply('Task has been moved');
                        });
                    }).catch(() => {
                        new Board(snowflakeNew).save().then(newBoard => {
                            task.board = newBoard;
                            task.save().then(task => {
                                interaction.reply('Task has been moved');
                            });
                        })
                    });
                });
            }).catch(() => {
                interaction.reply(`Could not find task ${task}`);
            });
        }).catch(() => {
            interaction.reply(`Could not find the board`);
        })
    }

}