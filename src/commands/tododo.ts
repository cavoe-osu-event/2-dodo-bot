import {SlashCommandBuilder} from "@discordjs/builders";
import {New} from "./new";
import {List} from "./list";
import {Assign} from "./assign";
import {Remove} from "./remove";
import {Progress} from "./progress";
import {Deadline} from "./deadline";

export class Tododo{

    public static getBuilder(): SlashCommandBuilder {
        return <SlashCommandBuilder>new SlashCommandBuilder()
            .setName('2dodo')
            .setDescription('2dodo commands')
            .addSubcommand(New.getBuilder)
            .addSubcommand(List.getBuilder)
            .addSubcommand(Assign.getBuilder)
            .addSubcommand(Remove.getBuilder)
            .addSubcommand(Progress.getBuilder)
            .addSubcommand(Deadline.getBuilder);
    }

}