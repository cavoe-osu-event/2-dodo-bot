import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, EmbedFieldData, GuildMember, MessageEmbed, Role, User} from "discord.js";
import {Board} from "../entity/board";
import {Task} from "../entity/task";

export class List {

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('list')
            .setDescription('Show tasks')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose to which persons or roles board gets shown')
                .setRequired(true));
    }

    public static handleInteraction(interaction: CommandInteraction) {
        const mentionable = interaction.options.getMentionable('board');

        let snowflake, name, color;
        if (mentionable instanceof Role ) {
            snowflake = mentionable.id;
            name = mentionable.name;
            color = mentionable.hexColor;
        } else if(mentionable instanceof User){
            snowflake = mentionable.id;
            name = mentionable.username;
            color = '#ffffff';
        } else if( mentionable instanceof GuildMember){
            snowflake = mentionable.id;
            name = mentionable.displayName
            color = '#ffffff';
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        Board.findOne({where: {mentionable: snowflake}}).then(board => {
            Task.find({where: {board: board }}).then(tasks => {

                let newTasks = tasks.filter(task => task.status === "NEW");
                let inProgressTasks = tasks.filter(task => task.status === "IN_PROGRESS");
                let doneTasks = tasks.filter(task => task.status === "DONE");

                newTasks.sort(List.compare);
                inProgressTasks.sort(List.compare);
                doneTasks.sort(List.compare);

                let newTasksValue = '==================\n';
                let inProgressTasksValue = '==================\n';
                let doneTasksValue = '==================\n';

                newTasks.forEach(task => {
                    newTasksValue = newTasksValue + `
                    ${task.id}: ${task.description}
                    ❗ Priority: ${task.priority}
                    ${task.dueDate ? '🕐 Due date: ' + new Date(task.dueDate).toDateString() : ''}
                    `
                });

                inProgressTasks.forEach(task => {
                    inProgressTasksValue = inProgressTasksValue + `
                    ${task.id}: ${task.description}
                    ❗ Priority: ${task.priority}
                    ${task.dueDate ? '🕐 Due date: ' + new Date(task.dueDate).toDateString() : ''}
                    `
                });

                doneTasks.forEach(task => {
                    doneTasksValue = doneTasksValue + `
                    ${task.id}: ${task.description}
                    ❗ Priority: ${task.priority}
                    ${task.dueDate ? '🕐 Due date: ' + new Date(task.dueDate).toDateString() : ''}
                    `
                });

                let fields = [
                    {name: 'New Tasks', value: newTasksValue, inline: true},
                    {name: 'In Progress Tasks', value: inProgressTasksValue, inline: true},
                    {name: 'Done Tasks', value: doneTasksValue, inline: true}
                    ];

                const embed = new MessageEmbed()
                    .setColor(color)
                    .setTitle('Board for ' + name)
                    .addFields(fields)
                    .setTimestamp();

                interaction.reply({embeds: [embed]});
            });
        }).catch(() => {
            interaction.reply(`Could not find the board`);
        })
    }

    private static compare(task1: Task, task2: Task) {
        if (task1.priority === "LOW" && ( task2.priority === "MEDIUM" || task2.priority === "HIGH") || task1.priority === "MEDIUM" && task2.priority === "HIGH") {
            return 1;
        }
        if (task2.priority === "LOW" && ( task1.priority === "MEDIUM" || task1.priority === "HIGH") || task2.priority === "MEDIUM" && task1.priority === "HIGH") {
            return -1;
        }
        return 0;
    }

}