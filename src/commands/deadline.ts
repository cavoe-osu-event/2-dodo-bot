import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, GuildMember, Role, User} from "discord.js";
import {Board} from "../entity/board";
import {Task} from "../entity/task";

export class Deadline {

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('due')
            .setDescription('Set a new due date for a task')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose the board the task is in')
                .setRequired(true))
            .addStringOption(builder => builder.setName('task')
                .setDescription('Choose which task to progress')
                .setRequired(true))
            .addStringOption(builder => builder.setName('due_date')
                .setDescription('Give an ISO 8601 formatted date that the task should be done by'));
    }

    public static handleInteraction(interaction: CommandInteraction) {
        const mentionable = interaction.options.getMentionable('board');

        let snowflake;
        if (mentionable instanceof GuildMember || mentionable instanceof User || mentionable instanceof Role ) {
            snowflake = mentionable.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        const task = interaction.options.getString('task');
        let dueDate;
        try {
            dueDate = Date.parse(interaction.options.getString('due_date'));
        }  catch (e){
            interaction.reply(`${interaction.options.getString('due_date')} is not a valid date. Please format it like this YYYY-MM-DDTHH:mm:ss`)
            return;
        }

        Board.findOne({where: {mentionable: snowflake}}).then(board => {
            Task.findOne({where: {id: task, board: board }}).then(task => {
                task.dueDate = dueDate;
                task.save().then(task => interaction.reply('Task has been updated'));
            }).catch(() => {
                interaction.reply(`Could not find task ${task}`);
            });
        }).catch(() => {
            interaction.reply(`Could not find the board`);
        })
    }
}