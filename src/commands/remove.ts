import {SlashCommandSubcommandBuilder} from "@discordjs/builders";
import {CommandInteraction, GuildMember, Role, User} from "discord.js";
import {Board} from "../entity/board";
import {Task} from "../entity/task";

export class Remove {

    public static getBuilder(): SlashCommandSubcommandBuilder {
        return new SlashCommandSubcommandBuilder()
            .setName('remove')
            .setDescription('Remove a task')
            .addMentionableOption(builder => builder.setName('board')
                .setDescription('Choose the board the task is in')
                .setRequired(true))
            .addStringOption(builder => builder.setName('task')
                .setDescription('Choose which task to remove')
                .setRequired(true))
    }

    public static handleInteraction(interaction: CommandInteraction) {
        const mentionable = interaction.options.getMentionable('board');

        let snowflake;
        if (mentionable instanceof GuildMember || mentionable instanceof User || mentionable instanceof Role ) {
            snowflake = mentionable.id;
        } else {
            interaction.reply(`Mentionable isn't an user or a role`);
            return;
        }

        const task = interaction.options.getString('task');

        Board.findOne({where: {mentionable: snowflake}}).then(board => {
            Task.findOne({where: {id: task, board: board }}).then(task => {
                task.remove().then(() => interaction.reply('Task has been removed'));
            }).catch(() => {
                interaction.reply(`Could not find task ${task}`);
            });
        }).catch(() => {
            interaction.reply(`Could not find the board`);
        })
    }
}