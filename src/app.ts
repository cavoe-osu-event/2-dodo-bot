import {Log} from "./service/log";
import {Database} from "./service/database";
import {Discord} from "./service/discord";

export class App{

    public static readonly instance = new App();

    public readonly ready: Promise<boolean>;

    constructor() {
        this.ready = new Promise<boolean>((resolve,reject) => {
            //Do all async setups here
            Promise.all([Database.getInstance().ready]).then(() => {
                    Discord.getClient().then(() => {});
                })
            }).catch(error => {
                //If the app isn't ready exit
                process.exitCode = 1;
                throw error;
            });
    }

    private printStartUp(): void{
        Log.info("2Dodo To-Do List Bot                                            ");
        Log.info("================================================================");
        Log.info("Support this project at https://www.buymeacoffee.com/eierpflanze");
        Log.info("================================================================");
    }
}