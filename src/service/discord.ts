import {
    Client,
    Guild,
    Intents,
    Message as DiscordMessage,
    CommandInteraction, Interaction
} from 'discord.js'

import {Config} from "./config";
import {Log} from "./log";
import {REST} from "@discordjs/rest";
import {Routes} from "discord-api-types/v8";
import {Tododo} from "../commands/tododo";
import {New} from "../commands/new";
import {List} from "../commands/list";
import {Assign} from "../commands/assign";
import {Deadline} from "../commands/deadline";
import {Remove} from "../commands/remove";
import {Progress} from "../commands/progress";

export class Discord {

    private static client: Client;
    private static ready = false;
    private static token = Config.getInstance().get('discord.bot-token');
    private static clientId = Config.getInstance().get('discord.client-id');
    private static commands = [Tododo.getBuilder()].map(command => command.toJSON());

    public static async getClient(): Promise<Client> {
        return new Promise(resolve => {
            if (!Discord.ready) {
                const config = Config.getInstance();
                Discord.client = new Client({ intents: [Intents.FLAGS.GUILDS] });
                //Register default methods
                Discord.client.on('ready', Discord.onReady);
                Discord.client.on('message', Discord.onMessage);
                Discord.client.on('guildCreate', Discord.onGuildJoin)
                Discord.client.on('error', Discord.onError);
                Discord.client.on('interactionCreate', Discord.onInteraction);
                //Login
                Discord.client.login(config.get('discord.bot-token'))
                    .then(() => {
                        Log.info('Discord bot has successfully logged in');
                        Discord.ready = true;
                        resolve(Discord.client);
                    });
            } else {
                resolve(Discord.client);
            }
        });
    }

    //General stuff that should be checked all the time

    /**
     * Method executed when the bot is ready to do it's job
     */
    private static onReady = () => {

    }

    /**
     * Method executed when ever the bot receives any message
     * @param message
     */
    private static onMessage = (message: DiscordMessage) => {
    }

    private static onInteraction = (interaction: Interaction) => {
        if (!interaction.isCommand()) return;
        const commandInteraction = interaction as CommandInteraction;
        if (commandInteraction.commandName === '2dodo') {
            switch ( commandInteraction.options.getSubcommand() ) {
                case 'new':
                    New.handleInteraction(commandInteraction);
                    break;
                case 'list':
                    List.handleInteraction(commandInteraction);
                    break;
                case 'assign':
                    Assign.handleInteraction(commandInteraction);
                    break;
                case 'due':
                    Deadline.handleInteraction(commandInteraction);
                    break;
                case 'remove':
                    Remove.handleInteraction(commandInteraction);
                    break;
                case 'progress':
                    Progress.handleInteraction(commandInteraction);
                    break;
                default:
                    //Nothing
                    break;
            }
        }
    }

    /**
     * Method executed when the bot joins a guild
     * @param guild
     */
    private static onGuildJoin = (guild: Guild) => {

        const rest = new REST({ version: '9' }).setToken(Discord.token);
        rest.put(Routes.applicationGuildCommands(Discord.clientId, guild.id), { body: Discord.commands })
            .then(() => Log.info('Successfully registered application commands.'))
            .catch(Log.error);
    }

    private static onError = (error: Error) => {
        Log.error('Discord bot error: ' + error);
    }
}