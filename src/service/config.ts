import fs from "fs";
import yaml from "js-yaml";
import {Log} from "./log";

export class Config {

    private static singleton: Config;

    private readonly configObject: any;

    private constructor() {
        try {
            this.configObject = yaml.safeLoad(fs.readFileSync(`./config/${process.env.NODE_ENV}.yml`, 'utf8'));
        } catch (e) {
            Log.error(`Could not load config file ${process.env.NODE_ENV}.yml`)
            Log.error(e);
            process.exit(1);
        }
    }

    public static getInstance(): Config {
        if (!Config.singleton) {
            Config.singleton = new Config();
        }
        return Config.singleton;
    }

    public get(path: string): any {
        return path.split('.').reduce((p, c) => p && p[c] || null, this.configObject);
    }

}