import winston from "winston";
import winstonDailyRotateFile from "winston-daily-rotate-file";
import {Config} from "./config";

export abstract class Log {

    private static logger: winston.Logger;

    private static setLogger() : void {
        const config = Config.getInstance();

        Log.logger = winston.createLogger({
            transports: [
                new winston.transports.Console({level: "debug", format: winston.format.cli()}),
                new winstonDailyRotateFile({
                    dirname: "logs/",
                    filename: "%DATE%.log",
                    level: config.get("log.level"),
                }),
            ]
        });
    }

    public static info(msg: string){
        if(!Log.logger){
            Log.setLogger()
        }
        Log.logger.info(msg);
    }

    public static warn(msg: string){
        if(!Log.logger){
            Log.setLogger()
        }
        Log.logger.warn(msg);
    }

    public static error(msg: string){
        if(!Log.logger){
            Log.setLogger()
        }
        Log.logger.error(msg);
    }

    public static http(msg: string){
        if(!Log.logger){
            Log.setLogger()
        }
        Log.logger.http(msg);
    }

    public static debug(msg: string){
        if(!Log.logger){
            Log.setLogger()
        }
        Log.logger.debug(msg);
    }


}