import {Log} from "./log";
import {Connection, createConnection, ConnectionOptions} from "typeorm";
import {Config} from "./config";
import {Board} from "../entity/board";
import {Task} from "../entity/task";

export class Database{

    private static singleton: Database;

    public connection: Connection;
    public readonly ready: Promise<boolean>;

    private constructor() {
        this.ready = new Promise<boolean>((resolve) => {
            this.connectDb().then(connection => {
                this.connection = connection;
                resolve(true);
            });
        });
    }

    private connectDb(): Promise<Connection>{

        const config = Config.getInstance();

        const connectionOptions: ConnectionOptions = {
            type: "sqlite",
            database: "data/database.sqlite",
            synchronize: true,
            logging: false,
            entities: [ Board, Task ]
        };
        const connectionPromise = createConnection(connectionOptions);
        connectionPromise.then(connection => {
            Log.info("Database connection established");
        }).catch(error => {
            Log.error("Database connection failed: " + error);
        });
        return connectionPromise;
    }

    public static getInstance(): Database {
        if (!Database.singleton) {
            Database.singleton = new Database();
        }
        return Database.singleton;
    }
}