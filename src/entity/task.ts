import {BaseEntity, BeforeInsert, Column, Entity, ManyToOne, PrimaryColumn} from "typeorm";
import {Board} from "./board";
import {Snowflake} from "discord-api-types/v8";
import {Database} from "../service/database";

@Entity('task')
export class Task extends BaseEntity {

    @ManyToOne(() => Board, {primary: true, eager: true})
    board: Board;

    @PrimaryColumn({type: "int"})
    id: number;

    @Column()
    description: string;

    @Column()
    priority: string;

    @Column({nullable: true})
    dueDate: Date;

    @Column()
    status: string;

    public readonly ready: Promise<boolean>;

    constructor(mentionable: Snowflake, description: string, priority?: string, dueDate?: Date) {
        super();

        if(!mentionable){
            return;
        }

        this.description = description;
        this.priority = priority;
        this.dueDate = dueDate;
        this.status = "NEW";
        this.ready = new Promise<boolean>((resolve) => {
            Database.getInstance().ready.then(() => {
                Board.findOne({where: {mentionable: mentionable}})
                    .then(board => {
                        if(!board){
                            new Board(mentionable).save().then(board => {
                                this.board = board;
                                resolve(true);
                            });
                        } else {
                            this.board = board;
                            resolve(true);
                        }
                    })
                    .catch(() => {
                        new Board(mentionable).save().then(board => {
                            this.board = board;
                            resolve(true);
                        });
                    });
            });
        });
    }

    @BeforeInsert()
    async setId() {
        const max = await Task.createQueryBuilder("task")
            .innerJoin("task.board", "board", "board.mentionable = :board", { board: this.board.mentionable })
            .select("MAX(task.id)", "max")
            .getRawOne();
        this.id = parseInt(max.max) + 1;
        if(!this.id){
            this.id = 1;
        }
    }

}