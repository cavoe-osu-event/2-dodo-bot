import {BaseEntity, Entity, PrimaryColumn} from "typeorm";
import {Snowflake} from "discord-api-types/v8";

@Entity('board')
export class Board extends BaseEntity{

    @PrimaryColumn()
    mentionable: string;

    constructor(mentionable: Snowflake) {
        super();
        this.mentionable = mentionable;
    }

}